# centos_oracle-0空白机安装

## 参考网址

- [oracle 12c数据库安装centos 7](https://www.cnblogs.com/elfin/p/16744945.html)
https://blog.csdn.net/qq_34905631/article/details/128177915
- [Linux CentOS7 Oracle如何配置开机自启](https://blog.csdn.net/qq_52572621/article/details/126606678)
- [oracle12c下载地址](https://edelivery.oracle.com/)

## 设置主机名称

root用户：

``` sh
hostnamectl set-hostname  oracle1
```

## 创建安装oracle程序用户组

创建用户组：
单实例默认创建oinstall、dba、oper即可，rac数据库需要全部创建

root用户：

```sh
groupadd dba
groupadd oinstall
useradd -g oinstall -G dba oracle
passwd oracle

groupadd oinstall 
groupadd dba
groupadd oper

groupadd backupdba
groupadd dgdba
groupadd kmdba
groupadd racdba


#集群环境：
groupadd asmadmin
groupadd asmdba
groupadd asmoper

usermod -g oinstall -G dba,oper,asmdba oracle
```

## 关闭防火墙，下次启动时防火墙仍随系统启动而启动

root用户：

```sh
systemctl stop firewalld.service 
# 彻底永久关闭防火墙 
systemctl disable firewalld.service 
# 查看防火墙状态
systemctl status firewalld.service

关闭SELINUX防火墙，修改/etc/selinux/config配置文件，将“SELINUX=enforcing”改为“SELINUX=disabled”。
```

## 配置内核参数与资源限制

root用户：

```sh
vi /etc/sysctl.conf 注意这只是官方要求的最低要求，可根据需要进行增大。

fs.aio-max-nr = 1048576
fs.file-max = 6815744
kernel.shmall = 2097152
kernel.shmmax = 4294967295
kernel.shmmni = 4096
kernel.sem = 250 32000 100 128
net.ipv4.ip_local_port_range = 9000 65500
net.core.rmem_default = 262144
net.core.rmem_max = 4194304
net.core.wmem_default = 262144
net.core.wmem_max = 1048576

---或者执行命令添加----------------
echo "fs.aio-max-nr = 1048576" >> /etc/sysctl.conf
echo "fs.file-max = 6815744" >> /etc/sysctl.conf
echo "kernel.shmall = 2097152" >> /etc/sysctl.conf
echo "kernel.shmmax = 68719476736" >> /etc/sysctl.conf
echo "kernel.shmmni = 4096" >> /etc/sysctl.conf
echo "kernel.sem = 250 32000 100 128" >> /etc/sysctl.conf
echo "net.ipv4.ip_local_port_range = 9000 65000" >> /etc/sysctl.conf
echo "net.core.rmem_default=262144" >> /etc/sysctl.conf
echo "net.core.rmem_max=4194304" >> /etc/sysctl.conf
echo "net.core.wmem_default=262144" >> /etc/sysctl.conf
echo "net.core.wmem_max=1048576" >> /etc/sysctl.conf

#使参数生效

sysctl -p
```

## 修改系统配置(Shell限制)

修改/etc/security/limits.conf文件,在末尾添加:

root用户：

```sh
oracle soft nproc 2047
oracle hard nproc 16384
oracle soft nofile 1024
oracle hard nofile 65536
oracle soft stack 10240
oracle hard stack 32768 
oracle hard memlock 134217728 
oracle soft memlock 134217728
```

## 修改CentOS系统标识 (由于Oracle默认不支持CentOS)

root用户：

```sh
vim /etc/redhat-release
4.1、删除CentOS Linux release 7.9.2009 (Core)（快捷键dd），改成redhat-7
​​​​​​​redhat-7
cat /etc/centos-release
```

## 安装oracle需要的依赖包

root用户：

```sh
yum -y install binutils  compat-libcap1  compat-libstdc++-33  gcc-c++  glibc  glibc-devel  ksh libaio  libaio-devel  libgcc  libstdc++  libstdc++-devel  libXi  libXtst  make sysstat  unixODBC  unixODBC-devel

重新执行下如下命令防止漏包：
yum install binutils compat-libstdc++** elfutils-libelf elfutils-libelf-devel elfutils-libelf-devel-static gcc gcc-c++ glibc glibc-common glibc-devel glibc-headers kernel-headers ksh libaio libaio-devel libgcc libgomp libstdc++ libstdc++-devel make sysstat unixODBC unixODBC-devel

```
