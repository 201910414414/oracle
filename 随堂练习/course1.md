# 随堂练习1:共享连接会话数量限制

## 第1步：配置为允许共享连接模式

打开一个新的终端窗口运行以下命令
其中shared_server_sessions=5表示最多允许5个连接Session

```sh
$sqlplus / as sysdba
ALTER SYSTEM SET dispatchers="(PROTOCOL=TCP)(dispatchers=3)";
ALTER SYSTEM SET max_dispatchers=5;
ALTER SYSTEM SET shared_servers = 1;
ALTER SYSTEM SET max_shared_servers=5;
ALTER SYSTEM SET shared_server_sessions=5;
show parameter shared_server
show parameter dispatchers
```

## 第2步：登录6次，前5次成功，第6次失败

新开6个窗口，分别运行同一个命令，每运行一个命令，就在另一个窗口运行第3步lsnrctl service观察共享连接数量的变化。

```sh
$sqlplus hr/123@localhost:1521/pdborcl:shared
共享模式不影响专用模式，专用模式一直可用：
$sqlplus hr/123@localhost:1521/pdborcl
```

## 第3步：新窗口观察共享连接数量

上一步每次登录，都运行lsnrctl service，查看pdborcl实例的D000,D001,D002的链接数量变化

```sh
$lsnrctl
service
```

## 第4步：关闭共享连接模式(可选)

```sh
sqlplus / as sysdba
ALTER SYSTEM SET dispatchers="(PROTOCOL=TCP)(service=orclXDB)";
关闭后，下面的命令不能登录：
$sqlplus hr/123@localhost:1521/pdborcl:shared
```
