# 随堂练习2:设置tnsnames.ora

## 第1步：在 $ORACLE_HOME/network/admin/tnsnames.ora文件中添加

```text
PDBORCL_S =
  (DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))
    )
    (CONNECT_DATA =
      (SERVER = SHARED)
      (SERVICE_NAME = pdborcl)
    )
  )

PDBORCL_D =
  (DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521))
    )
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = pdborcl)
    )
  )
```

## 第2步：共享模式登录

```sh
$sqlplus hr/123@pdborcl_s
```

## 第3步：专用模式登录

```sh
$sqlplus hr/123@pdborcl_d
```
