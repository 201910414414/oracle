# centos_oracle最终安装

## 解压oracle用户的Downloads中的4个压缩文件

oracle用户：

```sh
cd ~/Downloads/
unzip oracle12c.zip 
unzip sqldeveloper-22.2.1.234.1810-no-jre.zip
gzip -d rlwrap-0.37.tar.gz 
tar -xf rlwrap-0.37.tar
gzip -d openjdk-11+28_linux-x64_bin.tar.gz
tar -xf openjdk-11+28_linux-x64_bin.tar

# 删除原有4个压缩文件，剩下4个目录：
rm *.tar
rm *.zip
ls
database  jdk-11  rlwrap-0.37  sqldeveloper
```

## 安装rlwrap

root用户：

```sh
#yum -y install readline* libtermcap-devel*
```

oracle用户：

```sh
cd ~/Downloads/rlwrap-0.37/
./configure && make
```

root用户：还是在rlwrap-0.37目录

```sh
make install
```

## 开始安装oracle12c

oracle用户：

```sh
cd ~/Downloads/database
./runInstaller
```

出现GUI界面，按下面的方式选择：

- 选择创建和配置数据库=>服务器类=>单实例安装=>高级安装=>企业版
一般用途/事务处理
- Oracle基目录：/home/oracle/app/oracle
- 软件位置：/home/oracle/app/oracle/product/12.2.0/dbhome_1
- 产品清单目录：/home/oracle/app/oraInventory
- 全局数据库名和Oracle系统标识符都不变:为orcl
- 勾选：创建为容器数据库，输入可插入数据名：pdborcl
- 字符集选择：使用Unicode(AL32UTF8)(U)
- 勾选：在数据库中安装示例方案，将自动生成hr人力资源管理系统
- 指定数据库文件位置：/home/oracle/app/oracle/oradata
- 勾选：启用恢复（可选）
- 勾选：对所有账户使用相同的口令，并输入口令：123
- 开始安装。
- 安装过程如果出现Oracle Net Services 配置失败的警告，点“重试”，就会成功。
- 最后所有都是成功，最后的页面是：

```text
Oracle Enterprise Manager Database Express URL: https://oracle1:5500/em
```

## 安装后配置

### root用户

在文件/etc/profile后面添加：

```sh
export ORACLE_HOME=/home/oracle/app/oracle/product/12.2.0/dbhome_1
export ORACLE_SID=orcl
export JAVA_HOME=/usr/java/jdk1.8.0_181-cloudera
export PATH=$JAVA_HOME/bin:$ORACLE_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JAVA_HOME/jre/lib/rt.jar
export NLS_DATE_FORMAT="yyyy-mm-dd HH24:MI:SS"
export NLS_LANG='SIMPLIFIED CHINESE_CHINA.AL32UTF8'
```

### oracle用户

在文件.bashrc尾部添加：

```sh
alias sqlplus='rlwrap sqlplus'
alias rman='rlwrap rman'
alias lsnrctl='rlwrap lsnrctl'
```

### 添加开机启动trigger存储过程

oracle用户：

```sh
sqlplus / as sysdba

#进入SQL>的sqlplus管理环境，执行以下命令

show pdbs

create or replace trigger open_all_pdbs
  after startup on database
  begin
       execute immediate 'alter pluggable database all open';
    end open_all_pdbs;
    /

exit
```

### 修改oratab

root用户：

```sh
vi /etc/oratab 
将下面的N改为Y
orcl:/home/oracle/app/oracle/product/12.2.0/dbhome_1:N
改为：
orcl:/home/oracle/app/oracle/product/12.2.0/dbhome_1:Y
```

### 添加oracle启动服务oracle.service

root用户：

```sh
vim /usr/lib/systemd/system/oracle.service

文件内容输入：

[Unit]
Description=Oracle RDBMS
After=network.target

[Service]
Type=simple
ExecStart=/usr/bin/su - oracle -c "/home/oracle/app/oracle/product/12.2.0/dbhome_1/bin/dbstart $ORACLE_HOME >> /tmp/oracle.log"
ExecReload=/usr/bin/su - oracle -c "/home/oracle/app/oracle/product/12.2.0/dbhome_1/bin/dbrestart >> /tmp/oracle.log"
ExecStop=/usr/bin/su - oracle -c "/home/oracle/app/oracle/product/12.2.0/dbhome_1/bin/dbshut $ORACLE_HOME >> /tmp/oracle.log"
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

### 重新加载服务配置文件

```sh
systemctl daemon-reload
```

### 测试启动/停止oracle

```sh
systemctl stop oracle
systemctl start oracle
```

### 最后设置开机自动启动

root用户

```sh
systemctl enable oracle
```

## 安装sqldeveloper

oracle用户：

```sh
cd sqldeveloper

./sqldeveloper.sh

显示以下信息：
Oracle SQL Developer
Copyright (c) 2005, 2021, Oracle and/or its affiliates. All rights reserved.


Found /usr/java/jdk1.8.0_181-cloudera to run this product, and the major version of this Java is 8.
The mandatory minimum major version to run this product is 11.
This product cannot run with this Java.

The version of the default JDK (/usr/java/jdk1.8.0_181-cloudera) is too low.
Type the full pathname of a JDK installation (or Ctrl-C to quit), the path will be stored in /home/oracle/.sqldeveloper/22.2.1/product.conf

#这时输入jdk11的目录：

/home/oracle/Downloads/jdk-11

这时就启动了sqldeveloper
```

## 设置sqldeveloper桌面快捷方式

oracle用户：

```sh
vi /home/oracle/Desktop/sqldeveloper.desktop
输入以下内容：

[Desktop Entry]
Name=sqldeveloper
Exec=/home/oracle/Downloads/sqldeveloper/sqldeveloper.sh %u
Type=Application
Icon=/home/oracle/Downloads/sqldeveloper/icon.png
Terminal=false
```

双击桌面启动，第一次弹出对话框一定要点按钮“trust and launch”

## 激活hr用户

oracle用户：

```sh
sqlplus system/123@pdborcl

ALTER USER "HR" ACCOUNT UNLOCK ;
ALTER USER "HR" IDENTIFIED BY 123 ;

以下用户可能不存在：
ALTER USER "SCOTT" ACCOUNT UNLOCK ;
ALTER USER "SCOTT" IDENTIFIED BY 123 ;
ALTER USER "SH" ACCOUNT UNLOCK ;
ALTER USER "SH" IDENTIFIED BY 123 ;
ALTER USER "BI" ACCOUNT UNLOCK ;
ALTER USER "BI" IDENTIFIED BY 123 ;
ALTER USER "OE" ACCOUNT UNLOCK ;
ALTER USER "OE" IDENTIFIED BY 123 ;
ALTER USER "IX" ACCOUNT UNLOCK ;
ALTER USER "IX" IDENTIFIED BY 123 ;
ALTER USER "PM" ACCOUNT UNLOCK ;
ALTER USER "PM" IDENTIFIED BY 123 ;
```

### oracle安装完成后的配置文件内容

目录/home/oracle/app/oracle/product/12.2.0/dbhome_1/network/admin中

- tnsnames.ora的内容是：

```sh
LISTENER_ORCL =
  (ADDRESS = (PROTOCOL = TCP)(HOST = oracle1)(PORT = 1521))


ORCL =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = oracle1)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = orcl)
    )
  )

PDBORCL =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = oracle1)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = pdborcl)
    )
  )

ORCL =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = oracle1)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = orcl)
    )
  )
```

- 文件listener.ora的内容是：

```sh
LISTENER =
  (DESCRIPTION_LIST =
    (DESCRIPTION =
      (ADDRESS = (PROTOCOL = TCP)(HOST = oracle1)(PORT = 1521))
      (ADDRESS = (PROTOCOL = IPC)(KEY = EXTPROC1521))
    )
  )
```sh
