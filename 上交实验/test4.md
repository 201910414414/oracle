# 实验4：PL/SQL语言打印杨辉三角
 姓名：李润民          学号：201910414414    班级：20软工4班
 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 杨辉三角源代码

```
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```



创建存储过程：

```
create or replace PROCEDURE YHTRLANGLE(p_rows IN INTEGER) AS
type t_number is varray (100) of integer not null;
i integer;
j integer;
spaces varchar2(30) :='   '; 
N integer := p_rows; 
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); 
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put_line(''); 
   
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N 
    loop
        rowArray(i):=1;    
        j:=i-1;
           
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));
        end loop;
        dbms_output.put_line(''); 
    end loop;
END;
```



在hr用户进行存储过程的调用：

```
set SERVEROUTPUT on
begin
YHTRLANGLE(5);
end;·
```
# 实验总结：
我了解了杨辉三角的生成方法，即第n行的元素，等于第n-1行相邻两个元素的和。这个规律可以通过递归算法来实现。接着，我使用PL/SQL语言编写了递归函数，用于生成杨辉三角的每一行，直到达到指定的行数。

在编写递归函数的过程中，我深刻地理解了PL/SQL语言的特点和用法。PL/SQL是一种强类型的编程语言，具有很高的安全性和稳定性，同时也支持面向对象的编程方式。在编写递归函数时，我需要通过类型声明和参数传递来确保函数的正确性和可靠性。同时，我还需要使用控制流语句和循环语句来实现递归算法的逻辑。

最后，我使用PL/SQL语言编写了主程序，用于调用递归函数生成杨辉三角，并将结果输出到屏幕上。在输出结果时，我还使用了格式化字符串和打印控制语句，以便使结果更加清晰和易读。
